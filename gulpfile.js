'use strict';

var autoprefixer = require('autoprefixer');
var browserify = require('browserify');
//var buffer = require('vinyl-buffer');
var data = require('gulp-data');
var foreach = require('gulp-foreach');
var fs = require('fs');
var gulp = require('gulp');
var htmlhint = require('gulp-htmlhint');
var imagemin  = require('gulp-imagemin');
//var jshint = require('gulp-jshint');
var cleanCSS = require('gulp-clean-css');
var path = require('path');
var pngquant = require('imagemin-pngquant');
var postcss = require('gulp-postcss');
var prettify = require('gulp-prettify');
var removeEmptyLines = require('gulp-remove-empty-lines');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
//var source = require('vinyl-source-stream');
var sourcemaps = require('gulp-sourcemaps');
var swig = require('gulp-swig');
//var uglify = require('gulp-uglify');


gulp.task('default', ['templates', 'styles', 'images']);

//['templates', 'scripts', 'styles', 'fonts', 'images', 'lint']
gulp.task('watch', ['templates', 'styles', 'images'], function(){
	gulp.watch(['src/templates/data/**/**/*.json', 'src/templates/data/**/*.swig', 'src/templates/views/*.html'], ['templates']);
	gulp.watch('src/scss/*.scss', ['styles']);
	gulp.watch('src/images/**/*', ['images']);
	//gulp.watch('./src/js/*.js', ['scripts', 'lint']);
});

gulp.task('templates', ['banners', 'landing_pages', 'widgets'], function(){
	return gulp.src('./src/templates/data/**/*.swig')
		.pipe(swig({
			defaults: {
				cache: false
			}
		}))
		.on('error', handleError)
		.pipe(prettify({
			indent_char: '	',
			indent_size: 1
		}))
		.pipe(removeEmptyLines())
		.pipe(htmlhint())
		.pipe(htmlhint.reporter())
		.pipe(gulp.dest('./build'));
});

gulp.task('banners', function() {
	return gulp.src('./src/templates/data/**/category_page_banners/*.json')
		.pipe(foreach(function(stream, file){
			var jsonFile = file;
			var jsonBasename = path.basename(jsonFile.path, path.extname(jsonFile.path));
			var match = /.*(\/[^\/]*\/[^\/]*)$/.exec(path.dirname(jsonFile.path));
			var jsonDirname = match[1];
			return gulp.src('./src/templates/views/banner.html')
				.pipe(data(JSON.parse(fs.readFileSync(jsonFile.path, "utf8") || '{}')))
				.pipe(swig({
					defaults: {
						cache: false
					}
				}))
				.on('error', handleError)
				.pipe(prettify({
					indent_char: '	',
					indent_size: 1
				}))
				.pipe(removeEmptyLines())
				//.pipe(htmlhint())
				//.pipe(htmlhint.reporter())
				.pipe(rename(function(htmlFile) {
					htmlFile.basename = jsonBasename;
					htmlFile.dirname = jsonDirname;
				}))
				.pipe(gulp.dest('./build'));
		}));
});
gulp.task('landing_pages', function() {
	return gulp.src('./src/templates/data/**/landing_pages/*.json')
		.pipe(foreach(function(stream, file){
			var jsonFile = file;
			var jsonBasename = path.basename(jsonFile.path, path.extname(jsonFile.path));
			var match = /.*(\/[^\/]*\/[^\/]*)$/.exec(path.dirname(jsonFile.path));
			var jsonDirname = match[1];
			var jsonData = JSON.parse(fs.readFileSync(jsonFile.path, "utf8") || '{}');
			return gulp.src('./src/templates/views/'+jsonData.landingPageTemplate)
				.pipe(data(jsonData))
				.pipe(swig({
					defaults: {
						cache: false
					}
				}))
				.on('error', handleError)
				.pipe(prettify({
					indent_char: '	',
					indent_size: 1
				}))
				.pipe(removeEmptyLines())
				//.pipe(htmlhint())
				//.pipe(htmlhint.reporter())
				.pipe(rename(function(htmlFile) {
					htmlFile.basename = jsonBasename;
					htmlFile.dirname = jsonDirname;
				}))
				.pipe(gulp.dest('./build'));
		}));
});
gulp.task('widgets', function() {
	return gulp.src('./src/templates/data/**/lookbook_widgets/*.json')
		.pipe(foreach(function(stream, file){
			var jsonFile = file;
			var jsonBasename = path.basename(jsonFile.path, path.extname(jsonFile.path));
			var match = /.*(\/[^\/]*\/[^\/]*)$/.exec(path.dirname(jsonFile.path));
			var jsonDirname = match[1];
			return gulp.src('./src/templates/views/widget.html')
				.pipe(data(JSON.parse(fs.readFileSync(jsonFile.path, "utf8") || '{}')))
				.pipe(swig({
					defaults: {
						cache: false
					}
				}))
				.on('error', handleError)
				.pipe(prettify({
					indent_char: '	',
					indent_size: 1
				}))
				.pipe(removeEmptyLines())
				//.pipe(htmlhint())
				//.pipe(htmlhint.reporter())
				.pipe(rename(function(htmlFile) {
					htmlFile.basename = jsonBasename;
					htmlFile.dirname = jsonDirname;
				}))
				.pipe(gulp.dest('./build'));
		}));
});


gulp.task('styles', function(){
	return gulp.src('./src/scss/*.scss')
		//.pipe(sourcemaps.init())
		.pipe(sass())
		.on('error', handleError)
		.pipe(postcss([
			autoprefixer({
				browsers: ['last 3 versions', 'IE 8', 'IE 9', 'IE 10']
			})
		]))
		.pipe(gulp.dest('./build/assets/css'))
		.pipe(cleanCSS({
			compatibility: 'ie8'
		}))
		.pipe(rename({suffix: '.min'}))
		//.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('./build/assets/css'));
});

gulp.task('images', function(){
	return gulp.src('./src/images/**/*')
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.on('error', handleError)
		.pipe(gulp.dest('./build/assets/images'));
});

gulp.task('scripts', function(){
	return browserify('./src/js/index.js')
		.bundle()
		.on('error', handleError)
		.pipe(source('bundle.js'))
		.pipe(buffer())
		.pipe(uglify())
		.pipe(gulp.dest('./build/assets/js'));
});

gulp.task('lint', function(){
	return gulp.src('./src/js/*.js')
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

function handleError(err) {
	console.log(err.toString());
	this.emit('end');
}

